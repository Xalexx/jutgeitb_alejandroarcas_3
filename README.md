# JutgeITB 3.0

## Descripción

Bienvenidos al proyecto financiado por el departamento de programacion de l’Institut Tecnològic de Barcelona, este proyecto se basa en facilitar a los estudiantes de las diferentes asignaturas de programacion su aprendizaje.
Esta aplicacion ha sido mejorada durante muchos meses y por fin, tenemos el resultado final. Es una aplicacion super completa, ya que contiene una base de datos donde se guardan todos los profesores, problemas, notas de los estudiantes, etc...
Ademas los alumnos pueden practicar sus habilidades de programacion con la maravillosa funcionalidad del itinerario de problemas que se encuentra en el menu del alumnado, entre otras muchas mas funcionalidades. Creo que esta es una buena breve introduccion, lo demás lo dejo en manos de vosotros.

## Uso

- Para empezar deberás tener una copia de la base de datos, para poder tener unos problemas iniciales con los que practicar, para ello deberás descargar este script, que contiene todas las sentencias sql necesarias para poder introducirte en la aplicacion correctamente. [Descargar Script](https://drive.google.com/file/d/13O_-fi7-7RJyvGsOKjX57-ZdaO_jUghS/view?usp=share_link)
- Una vez tengas descargado el script una de las formas para ejecutar el contenido del mismo es copiandolo y ejecutarlo en la consola de postgres psql.
- ¿Como hacer eso? Muy sencillo, te lo explico por pasos:


    - Primero deberás abrir la terminal de postgres e iniciar sesion con el usuario postgres

  ![](./fotos/foto1.PNG)


    - Una vez iniciado sesion deberá aparecer algo similar a lo siguiente:


  ![](./fotos/foto2.PNG)


    - Ya casi estamos... Ahora simplemente deberás pegar el contenido del script en prompt, de esta manera:


  ![](./fotos/foto3.PNG)


    - Ahora ya tenemos preparada la base de datos para iniciar el jutgeITB, simplemente queda compilar el proyecto y ejecutar...

#### A DISFRUTAR

## Creditos

- Alejandro Arcas Leon


