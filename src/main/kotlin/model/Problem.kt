package model

import blueColor
import connection
import greenColor
import org.postgresql.util.PSQLException
import redColor
import resetColor
import sc
import java.sql.SQLException

class Problem(val problemId: Int, val theme: String, val enunciat: String, var solved: Boolean, var attempts: Attempt): Crud<Problem> {

    fun buildProblem(): String {
        var wantToPlay: String
        println("\n${blueColor}Problema ${problemId}$resetColor: ${theme}\n${enunciat}\n")
        println("${blueColor}Juego publico:$resetColor")
        getInfoPublicTestingGame(problemId)
        println("\n${redColor}Quieres resolver el problema?\n${blueColor}1 $resetColor-> ${greenColor}Si    ${blueColor}2 $resetColor-> ${greenColor}No$resetColor")
        wantToPlay = ""
        while (wantToPlay != "1" && wantToPlay != "2"){
            print("Introduce ${redColor}una respuesta$resetColor valida: ")
            wantToPlay = sc.next()
        }
        return wantToPlay
    }

    fun playProblem() {
        var userAnswer: String
        println("\n${blueColor}Juego privado:$resetColor\nEntrada: ${getInfoPrivateTestingGame(problemId)[0].input}    Salida: ...")
        while (true){
            print("Introduce la repuesta correcta: ")
            userAnswer = sc.next()
            attempts.attempt++
            if (userAnswer == getInfoPrivateTestingGame(problemId)[0].output) break
            else println("\n${redColor}Respuesta incorrecta$resetColor.\n")
        }
        println("\n${greenColor}Enhorabuena, has acertado la respuesta correcta!$resetColor")
        updateData()
    }

    private fun getInfoPublicTestingGame(problemId: Int){
        val listOfPublicTestingGame = mutableListOf<PublicTestingGame>()
        val sentence = "SELECT input, output FROM public_testing_game WHERE problem_id = ?"
        val query = connection.prepareStatement(sentence)
        query.setInt(1, problemId)
        val result = query.executeQuery()
        while (result.next()){
            listOfPublicTestingGame.add(PublicTestingGame(result.getString("input"), result.getString("output"), problemId))
        }
        for (i in listOfPublicTestingGame.indices){
            print("Input: ${listOfPublicTestingGame[i].input}    Output: ${listOfPublicTestingGame[i].output}\n")
        }
    }

    private fun getInfoPrivateTestingGame(problemId: Int): List<PrivateTestingGame> {
        val listOfPrivateTestingGame = mutableListOf<PrivateTestingGame>()
        val sentence = "SELECT input, output FROM private_testing_game WHERE problem_id = ?"
        val query = connection.prepareStatement(sentence)
        query.setInt(1, problemId)
        val result = query.executeQuery()
        while (result.next()) {
            listOfPrivateTestingGame.add(PrivateTestingGame(result.getString("input"), result.getString("output"), problemId))
        }
        return listOfPrivateTestingGame
    }

    override fun insertData(): Boolean {
        var inserted = false
        val sentence = "INSERT INTO problem (problem_id, theme, enunciat, solved, attempts) VALUES (?, ?, ?, ?, ?)"
        try {
            val query = connection.prepareStatement(sentence)
            query.setInt(1, this.problemId)
            query.setString(2, this.theme)
            query.setString(3, this.enunciat)
            query.setBoolean(4, this.solved)
            query.setInt(5, this.attempts.attempt)
            query.executeUpdate()
            inserted = true
        } catch (e: SQLException){
            println("Error: " + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error: " + e.errorCode + e.message)
        }
        return inserted
    }

    override fun readData(): Problem? {
        TODO("Not yet implemented")
    }

    override fun updateData() {
        val sentence = "UPDATE problem SET attempts = ?, solved = ? WHERE problem_id = ?"
        try {
            val query = connection.prepareStatement(sentence)
            query.setInt(1, attempts.attempt)
            query.setBoolean(2, true)
            query.setInt(3, problemId)
            query.executeUpdate()
        } catch (e: SQLException){
            println("Error: " + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error: " + e.errorCode + e.message)
        }

    }

    override fun deleteData(): Boolean {
        TODO("Not yet implemented")
    }

    override fun checkIfExist(entity: Problem): Boolean {
        TODO("Not yet implemented")
    }
}