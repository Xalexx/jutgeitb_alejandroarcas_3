package model

interface Crud<T> {
    fun insertData(): Boolean
    fun readData(): T?
    fun updateData()
    fun deleteData(): Boolean
    fun checkIfExist(entity: T): Boolean
}