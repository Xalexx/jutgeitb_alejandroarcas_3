package model

import connection
import org.postgresql.util.PSQLException
import java.sql.SQLException

class Score(val score: Int?, val teacher: String) {

    fun addToDataBase(): Boolean {
        var exists = false
        var inserted = false
        var score = 0
        try {
            val query = connection.createStatement()
            val checkSentence = "SELECT * FROM score WHERE teacher_name = '${this.teacher}'"
            val result = query.executeQuery(checkSentence)
            while (result.next()){
                if (result.getString("teacher_name") == this.teacher) {
                    exists = true
                    score = result.getInt("score")
                    break
                }
            }
        } catch (e: SQLException){
            println("Error: " + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error: " + e.errorCode + e.message)
        }
        if (exists){
            val sentence = "UPDATE score SET score = ? WHERE teacher_name = ?"
            try {
                val query = connection.prepareStatement(sentence)
                query.setInt(1, score + this.score!!)
                query.setString(2, this.teacher)
                query.executeUpdate()
                inserted = true
            } catch (e: SQLException){
                println("Error: " + e.errorCode + e.message)
            } catch (e: PSQLException){
                println("Error: " + e.errorCode + e.message)
            }
        } else {
            val sentence = "INSERT INTO score (score, teacher_name) VALUES (?, ?)"
            try {
                val query = connection.prepareStatement(sentence)
                query.setInt(1, this.score!!)
                query.setString(2, this.teacher)
                query.executeUpdate()
                inserted = true
            } catch (e: SQLException){
                println("Error: " + e.errorCode + e.message)
            } catch (e: PSQLException){
                println("Error: " + e.errorCode + e.message)
            }
        }
        return inserted
    }

    fun deleteFromDataBase(): Boolean {
        var deleted = false
        val sentence = "DELETE FROM score WHERE teacher_name = ?"
        try {
            val query = connection.prepareStatement(sentence)
            query.setString(1, this.teacher)
            query.executeUpdate()
            deleted = true
        } catch (e: SQLException){
            println("Error:" + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error:" + e.errorCode + e.message)
        }
        return deleted
    }
}
