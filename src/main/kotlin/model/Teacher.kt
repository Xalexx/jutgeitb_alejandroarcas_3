package model

import connection
import greenColor
import kotlinx.serialization.Serializable
import org.postgresql.util.PSQLException
import resetColor
import java.sql.SQLException

@Serializable
class Teacher(private val name: String, private val password: String): Crud<Teacher> {

    override fun insertData(): Boolean {
        var inserted = false
        if (!checkIfExist(Teacher(this.name, this.password))){
            val sentence = "INSERT INTO teacher (teacher_name, teacher_pass) VALUES (?, ?)"
            try {
                val query = connection.prepareStatement(sentence)
                query.setString(1, this.name)
                query.setString(2, this.password)
                query.executeUpdate()
                println("\nDocente ${greenColor}agregado$resetColor correctamente.")
            } catch (e: SQLException){
                println("Error: " + e.errorCode + e.message)
            } catch (e: PSQLException){
                println("Error: " + e.errorCode + e.message)
            }
            val secondSentence = "INSERT INTO score (score, teacher_name) VALUES (?, ?)"
            try {
                val query = connection.prepareStatement(secondSentence)
                query.setInt(1, 0)
                query.setString(2, this.name)
                query.executeUpdate()
            } catch (e: SQLException){
                println("Error: " + e.errorCode + e.message)
            } catch (e: PSQLException){
                println("Error: " + e.errorCode + e.message)
            }
        } else {
            println("\nError: El docente con el nombre o contraseña indicados ya existe.")
            inserted = true
        }
        return inserted
    }

    override fun readData(): Teacher? {
        var teacherObject: Teacher? = null
        val sentence = "SELECT * FROM teacher WHERE teacher_name = ? AND teacher_pass = ?"
        try {
            val query = connection.prepareStatement(sentence)
            query.setString(1, this.name)
            query.setString(2, this.password)
            val result = query.executeQuery()
            while (result.next()){
                val resultName = result.getString("teacher_name")
                val resultPass = result.getString("teacher_pass")
                teacherObject = Teacher(resultName, resultPass)
            }
        } catch (e: SQLException){
            println("Error: " + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error: " + e.errorCode + e.message)
        }
        return teacherObject
    }

    override fun deleteData(): Boolean {
        var deleted = false
        val sentence = "DELETE FROM teacher WHERE teacher_pass = ?"
        try {
            val query = connection.prepareStatement(sentence)
            query.setString(1, this.password)
            query.executeUpdate()
            deleted = true
        } catch (e: SQLException){
            println("Error:" + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error:" + e.errorCode + e.message)
        }
        return deleted
    }

    override fun checkIfExist(entity: Teacher): Boolean {
        var exists = false
        val sentence = "SELECT teacher_name, teacher_pass FROM teacher"
        try {
            val query = connection.prepareStatement(sentence)
            val result = query.executeQuery()
            while (result.next()){
                val resultPass = result.getString("teacher_pass")
                val resultName = result.getString("teacher_name")
                if (resultPass == entity.password || resultName == entity.name) {
                    exists = true
                    break
                }
            }
        } catch (e: SQLException){
            println("Error:" + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error:" + e.errorCode + e.message)
        }
        return exists
    }

    override fun updateData() {
        TODO("Not yet implemented")
    }
}