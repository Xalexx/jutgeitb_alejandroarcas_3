package model

import connection
import org.postgresql.util.PSQLException
import java.sql.SQLException

class PublicTestingGame(val input: String, val output: String, private val problemId: Int) {
    fun addToDataBase(): Boolean {
        var inserted = false
        val sentence = "INSERT INTO public_testing_game (input, output, problem_id) VALUES (?, ?, ?)"
        try {
            val query = connection.prepareStatement(sentence)
            query.setString(1, this.input)
            query.setString(2, this.output)
            query.setInt(3, this.problemId)
            query.executeUpdate()
            inserted = true
        } catch (e: SQLException){
            println("Error: " + e.errorCode + e.message)
        } catch (e: PSQLException){
            println("Error: " + e.errorCode + e.message)
        }
        return inserted
    }

}