import model.*
import org.postgresql.util.PSQLException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.Scanner
import kotlin.system.exitProcess

/* Colors Initialization */
const val redColor = "\u001b[31m"
const val blueColor = "\u001B[34m"
const val greenColor = "\u001B[32m"
const val resetColor = "\u001b[0m"

/* Global variables */
val sc = Scanner(System.`in`)
const val studentPass = "alumneITB"
const val databaseURL = "jdbc:postgresql://localhost:5432/jutgedb"
lateinit var connection: Connection

fun main() {
    try {
        connection = DriverManager.getConnection(databaseURL, "postgres", "postgres")
        connection.isValid(0)

        print("Bienvenido a$redColor JutgeITB 3.0$resetColor")
        while (true) {
            println("\nSelecciona$redColor UNA$resetColor de las siguientes opciones:")
            println("${blueColor}1$resetColor ->$greenColor Alumno/a\n" +
                    "${blueColor}2$resetColor ->$greenColor Docente\n" +
                    "${blueColor}3$resetColor ->$greenColor Ayuda\n" +
                    "${blueColor}4$resetColor ->$greenColor Salir de la aplicacion$resetColor")
            print("Introduce aqui tu respuesta: ")
            when (sc.next()) {
                "1" -> {
                    // Access with Password
                    println("\nPara acceder al ${redColor}menú de Alumno$resetColor introduce la ${redColor}contraseña$resetColor.")
                    do {
                        print("Introduce tu respuesta aquí: ")
                        val studentAnswer = sc.next()
                        if (studentAnswer != studentPass) println("\nEscribe la$redColor contraseña$resetColor correcta:")
                    } while (studentAnswer != studentPass)
                    // Menu selection
                    while (true) {
                        println("\n${redColor}Bienvenido/a ${blueColor}Alumno$redColor\n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾$resetColor")
                        println("${blueColor}1$resetColor ->$greenColor Seguir con el itinerario de problemas\n" +
                                "${blueColor}2$resetColor ->$greenColor Mostrar la lista de problemas\n" +
                                "${blueColor}3$resetColor ->$greenColor Mostrar el historico de problemas resueltos\n" +
                                "${blueColor}4$resetColor ->$greenColor Atras$resetColor")
                        print("Introduce aqui tu respuesta: ")
                        when (sc.next()) {
                            "1" -> {
                                sc.nextLine()
                                var theme: String
                                do {
                                    println("\nEscoge un tema para los problemas (${redColor}Tipos de datos, ${blueColor}Condicionales, ${resetColor}Bucles, ${greenColor}Listas$resetColor, Todos):")
                                    print("Introduce aquí tu respuesta: ")
                                    theme = sc.nextLine()
                                } while (theme != "Tipos de datos" && theme != "Condicionales" && theme != "Bucles" && theme != "Listas" && theme != "Todos")
                                val listOfProblems = itineraryOfProblems(theme)
                                if (listOfProblems.isEmpty()) println("\nNo hay ${redColor}problemas$resetColor a resolver")
                                else {
                                    for (i in listOfProblems.indices){
                                        val problem = Problem(
                                            listOfProblems[i].problemId,
                                            listOfProblems[i].theme,
                                            listOfProblems[i].enunciat,
                                            listOfProblems[i].solved,
                                            listOfProblems[i].attempts
                                        )
                                        if (problem.buildProblem() == "1") problem.playProblem()
                                        else {
                                            println("\n¡Nos vemos a la próxima!")
                                            break
                                        }
                                    }
                                }
                            }
                            "2" -> {
                                var userAnswer: String
                                do {
                                    println("\nQue tipo de ${redColor}problemas$resetColor deseas ${redColor}mostrar$resetColor?")
                                    println("${blueColor}1$resetColor -> ${greenColor}Resueltos\n${blueColor}2$resetColor -> ${greenColor}NO Resueltos$resetColor")
                                    print("Introduce aquí tu respuesta: ")
                                    userAnswer = sc.next()
                                } while (userAnswer !in "1".."2")
                                val listOfProblems = showListOfProblems(userAnswer)
                                if (listOfProblems.isEmpty()) println("\nNo hay ${redColor}problemas$resetColor para ${redColor}mostrar$resetColor")
                                else {
                                    for (i in listOfProblems.indices){
                                        print("\n${blueColor}Problema ${listOfProblems[i].problemId}$resetColor -> $greenColor${listOfProblems[i].attempts.attempt} intentos$resetColor")
                                    }
                                    println()
                                    println("\n${blueColor}¿Quieres resolver algun problema de los mostrados?$resetColor\nSelecciona el ${redColor}numero$resetColor del problema a resolver,")
                                    println("si no quieres resolver ningun problema, escribe un ${redColor}0$resetColor")
                                    print("Introduce aquí tu respuesta: ")
                                    val userInput = sc.next()
                                    if (userInput != "0") {
                                        for (i in listOfProblems){
                                            if (i.problemId.toString() == userInput){
                                                val problem = Problem(i.problemId, i.theme, i.enunciat, i.solved, Attempt(0))
                                                if (problem.buildProblem() == "1") problem.playProblem()
                                                else {
                                                    println("\n¡Nos vemos a la próxima!")
                                                    break
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            "3" -> {
                                println("\nDeseas ${redColor}ordenar$resetColor por numero de ${redColor}intentos$resetColor?")
                                println("${blueColor}1$resetColor -> ${greenColor}Sí\n${blueColor}2$resetColor -> ${greenColor}No$resetColor")
                                print("Introduce aquí tu respuesta: ")
                                var userAnswer = sc.next()
                                while (userAnswer !in "1".."2"){
                                    println("\nEscribe una ${redColor}respuesta válida$resetColor:\nDeseas ${redColor}ordenar$resetColor por numero de ${redColor}intentos$resetColor?")
                                    println("${blueColor}1$resetColor -> ${greenColor}Sí\n${blueColor}2$resetColor -> ${greenColor}No$resetColor")
                                    print("Introduce aquí tu respuesta: ")
                                    userAnswer = sc.next()
                                }
                                val list = showSolvedProblems(userAnswer)
                                if (list.isEmpty()) println("\nNo hay problemas ${redColor}resueltos$resetColor")
                                else {
                                    for (i in list.indices){
                                        print("\n${blueColor}Problema ${list[i].problemId}$resetColor -> $greenColor${list[i].attempts.attempt} intentos$resetColor")
                                    }
                                    println()
                                }
                            }
                            "4" -> break
                            else -> println("\nIntroduce ${redColor}UNA respuesta$resetColor válida.")
                        }
                    }
                }
                "2" -> {
                    // Login teacher
                    login@ while (true) {
                        var name: String?
                        var password: String?
                        println("\nPara acceder al menú de ${redColor}Docente$resetColor debes ${redColor}iniciar sesion$resetColor o ${redColor}registrarte$resetColor como docente.")
                        println("${blueColor}1$resetColor ->$greenColor Inicia sesion como docente\n" +
                                "${blueColor}2$resetColor ->$greenColor Registrate como docente\n" +
                                "${blueColor}3$resetColor ->$greenColor Atras$resetColor")
                        print("Introduce aqui tu respuesta: ")
                        when (sc.next()) {
                            "1" -> {
                                println("\n${redColor}Inicio de sesion\n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾$resetColor")
                                do {
                                    println("Introduce el ${redColor}nombre$resetColor del docente:")
                                    name = sc.next()
                                    println("Introduce el ${redColor}password$resetColor del docente:")
                                    password = sc.next()
                                    val teacher = Teacher(name, password).readData()
                                    if (teacher == null) {
                                        println("\n${redColor}Error:$resetColor nombre o contraseña incorrecto\n")
                                        var userAnswer: String
                                        do {
                                            println("Deseas ${redColor}volver$resetColor al menu? \n${blueColor}1$resetColor ->$greenColor Si\n${blueColor}2$resetColor ->$greenColor No$resetColor")
                                            print("Introduce aqui tu respuesta: ")
                                            userAnswer = sc.next()
                                        } while (userAnswer !in "1".."2")
                                        if (userAnswer == "1") break@login
                                        println()
                                    }

                                } while (teacher == null)
                                while (true) {
                                    println("\n${redColor}Bienvenido/a $blueColor$name$redColor\n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾$resetColor")
                                    println("${blueColor}1$resetColor ->$greenColor Añadir un nuevo problema\n" +
                                            "${blueColor}2$resetColor ->$greenColor Reportar las taeras de los alumnos\n" +
                                            "${blueColor}3$resetColor ->$greenColor Mostrar la puntuacion de cada docente a los alumnos\n" +
                                            "${blueColor}4$resetColor ->$greenColor Eliminar perfil de docente\n" +
                                            "${blueColor}5$resetColor ->$greenColor Atras$resetColor")
                                    print("Introduce aqui tu respuesta: ")
                                    when (sc.next()){
                                        "1" -> {
                                            val problemId = (getProblemId().size) + 1
                                            println("\nPara añadir un nuevo problema debes introducir la siguiente informacion")
                                            sc.nextLine()
                                            var theme :String
                                            do {
                                                println("Introduce una categoria correcta (${redColor}Tipos de datos, ${blueColor}Condicionales, ${resetColor}Bucles, ${greenColor}Listas$resetColor):")
                                                theme = sc.nextLine()
                                            } while (theme != "Tipos de datos" && theme != "Condicionales" && theme != "Bucles" && theme != "Listas")
                                            println("Escribe el ${redColor}titulo$resetColor:")
                                            val title = sc.nextLine()
                                            println("Escribe el ${redColor}enunciado$resetColor:")
                                            val enunciat = sc.nextLine()
                                            val problem = Problem(problemId, theme, "$title: $enunciat", false, Attempt(0)).insertData()

                                            println("Introduce el ${redColor}numero$resetColor de juegos de prueba publicos que ${redColor}deseas$resetColor en el problema:")
                                            val numberOfTestGame = sc.nextInt()
                                            sc.nextLine()
                                            var publicGame = false
                                            for (i in 1..numberOfTestGame){
                                                println("Introduce el juego de pruebas publico:\nEscribe el ${redColor}input$resetColor del juego de prueba $i:")
                                                val input = sc.nextLine()
                                                println("Escribe el ${blueColor}output$resetColor del juego de prueba $i:")
                                                val output = sc.nextLine()
                                                publicGame = PublicTestingGame(input, output, problemId).addToDataBase()
                                            }
                                            println("Introduce el juego de pruebas privado:\nEscribe el ${redColor}input$resetColor:")
                                            val input = sc.nextLine()
                                            println("Escribe el ${blueColor}output$resetColor:")
                                            val output = sc.nextLine()
                                            val privateGame = PrivateTestingGame(input, output, problemId).addToDataBase()

                                            if (problem && publicGame && privateGame) println("${redColor}Problema$resetColor añadido ${greenColor}correctamente$resetColor")
                                            else println("El${redColor}problema$resetColor no se ha ${greenColor}añadido$resetColor")
                                        }
                                        "2" -> {
                                            val listOfProblems = showListOfProblems("1")
                                            if (listOfProblems.isEmpty()) println("\nNo puedes ${redColor}introducir$resetColor una ${redColor}puntuación$resetColor porque todavía ${redColor}NO$resetColor hay problemas resueltos")
                                            else {
                                                for (i in listOfProblems.indices){
                                                    print("\n${blueColor}Problema ${listOfProblems[i].problemId}$resetColor -> $greenColor${listOfProblems[i].attempts.attempt} intentos$resetColor")
                                                }
                                                println()
                                                println("\n${redColor}Asigna una puntuacion para los alumnos:$resetColor")
                                                println("$blueColor$name$resetColor, indica la ${blueColor}puntuacion$resetColor que deseas asignar:")
                                                print("\nEscribe tu respuesta aqui: ")
                                                val problemScore = sc.nextInt()
                                                val score = Score(problemScore, name!!).addToDataBase()
                                                if (score) println("\n${redColor}Puntuación$resetColor añadida ${greenColor}correctamente$resetColor")
                                            }
                                        }
                                        "3" -> {
                                            println("\n${redColor}La puntuacion total de cada profesor para los alumnos:$resetColor")
                                            val listOfScores = getScoresInfo()
                                            if (listOfScores.isEmpty()) println("No se ha ${redColor}registrado$resetColor ninguna ${greenColor}puntuación$resetColor todavía.")
                                            else {
                                                for (i in listOfScores.indices){
                                                    print("$greenColor${listOfScores[i].teacher}$resetColor --> ")
                                                    if (listOfScores[i].score == 0) print("${redColor}No se ha registrado ninguna puntuacion$resetColor")
                                                    else print("$blueColor${listOfScores[i].score} pt$resetColor")
                                                    println()
                                                }
                                            }
                                        }
                                        "4" -> {
                                            println("\nDeseas ${redColor}eliminar$resetColor este ${redColor}perfil$resetColor de docente?")
                                            var decision: String
                                            do {
                                                println("${blueColor}1$resetColor ->$greenColor Si\n" +
                                                        "${blueColor}2$resetColor ->$greenColor No, volver atrás$resetColor")
                                                print("Introduce aqui tu respuesta: ")
                                                decision = sc.next()
                                            } while (decision !in "1".."2")
                                            if (decision == "1") {
                                                val deletedTeacher = Teacher(name!!, password!!).deleteData()
                                                val deletedTeacherScore = Score(null, name).deleteFromDataBase()
                                                if (deletedTeacher && deletedTeacherScore) println("\nDocente ${redColor}eliminado$resetColor correctamente")
                                                break@login
                                            }
                                        }
                                        "5" -> break
                                        else -> println("\nIntroduce ${redColor}UNA respuesta$resetColor válida.")
                                    }
                                }
                            }
                            "2" -> {
                                do {
                                    println("\n${redColor}Registro\n‾‾‾‾‾‾‾‾‾‾‾‾‾$resetColor")
                                    println("Introduce el ${blueColor}nombre$resetColor del ${redColor}nuevo$resetColor docente:")
                                    print("${redColor}Nota:$resetColor El ${redColor}nombre$resetColor no puede tener ${redColor}espacios en blanco$resetColor\n")
                                    val newTeacherName = sc.next()
                                    println("\nIntroduce el ${blueColor}password$resetColor del ${redColor}nuevo$resetColor docente:")
                                    print("${redColor}Nota:$resetColor El ${redColor}password$resetColor no puede tener ${redColor}espacios en blanco$resetColor\n")
                                    val newTeacherPass = sc.next()
                                    val exists = Teacher(newTeacherName, newTeacherPass).insertData()
                                } while (exists)

                            }
                            "3" -> break
                            else -> println("\nIntroduce ${redColor}UNA respuesta$resetColor válida.")
                        }
                    }
                }
                "3" -> {
                    helpMenu()
                    do {
                        println("${redColor}Volver atras:$resetColor")
                        println("${blueColor}1$resetColor ->$greenColor Si\n" +
                                "${blueColor}2$resetColor ->$greenColor No, salir de la aplicacion$resetColor")
                        print("Introduce aqui tu respuesta: ")
                        val helpAnswer = sc.next()
                        when (helpAnswer){
                            "1" -> break
                            "2" -> exitProcess(0)
                            else -> {
                                println("\nSelecciona$redColor UNA$resetColor opción válida:")
                            }
                        }
                    } while (helpAnswer !in "1".."2")
                }
                "4" -> break
                else -> println("\nIntroduce ${redColor}UNA respuesta$resetColor válida.")
            }
        }
        connection.close()

    } catch (e: PSQLException) {
        println("Error: ${e.errorCode}, ${e.message}")
    }
}

fun helpMenu() {
    println("\n${redColor}Bienvenido al menu de ayuda de JutgeITB 3.0$resetColor")
    println("Para utilizar este programa tenemos dos tipos de perfiles,\n" +
            "${blueColor}Alumno/a$resetColor o ${blueColor}Docente$resetColor " +
            "Si accedes como ${blueColor}Alumno/a$resetColor tendras la posibilidad de\n" +
            "iniciar un itinerario de problemas, de todo tipo, una vez hayas finalizado\n" +
            "un problema, tendras la correcion inmediata y ademas tendras un historico de\n" +
            "tus problemas resueltos y con sus intentos correspondientes.\n" +
            "Por otra parte tenemos el perfil de ${blueColor}Docente$resetColor donde tenemos\n" +
            "la posibilidad de sacar una puntuacion a los alumnos ya sea por los problemas\n" +
            "resueltos o por el numero de intentos, tambien esta la posibilidad de agregar\n" +
            "mas problemas para los ${blueColor}alumnos$resetColor y ponerlo un poquito mas complicado!")
}

private fun itineraryOfProblems(theme: String): List<Problem> {
    val listOfProblems = mutableListOf<Problem>()
    try {
        val query = connection.createStatement()
        val sentence: String = if (theme == "Todos") "SELECT * FROM problem WHERE solved = false ORDER BY problem_id"
        else "SELECT * FROM problem WHERE theme = '$theme' AND solved = false ORDER BY problem_id"
        val result = query.executeQuery(sentence)
        while (result.next()) {
            listOfProblems.add(
                Problem(
                    result.getInt("problem_id"),
                    result.getString("theme"),
                    result.getString("enunciat"),
                    false,
                    Attempt(result.getInt("attempts"))
                )
            )
        }
    } catch (e: SQLException){
        println("Error: " + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error: " + e.errorCode + e.message)
    }
    return listOfProblems
}

private fun showListOfProblems(solved: String): List<Problem> {
    val listOfProblems = mutableListOf<Problem>()
    try {
        val query = connection.createStatement()
        val sentence: String = if (solved == "1") "SELECT * FROM problem WHERE solved = true ORDER BY problem_id"
        else "SELECT * FROM problem WHERE solved = false ORDER BY problem_id"
        val result = query.executeQuery(sentence)
        while (result.next()){
            listOfProblems.add(
                Problem(
                    result.getInt("problem_id"),
                    result.getString("theme"),
                    result.getString("enunciat"),
                    result.getBoolean("solved"),
                    Attempt(result.getInt("attempts"))
                )
            )
        }
    } catch (e: SQLException){
        println("Error: " + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error: " + e.errorCode + e.message)
    }
    return listOfProblems
}

private fun showSolvedProblems(sorted: String): List<Problem> {
    val listOfProblemsSorted = mutableListOf<Problem>()
    try {
        val query = connection.createStatement()
        val sentence: String = if (sorted == "1") "SELECT * FROM problem WHERE solved = true ORDER BY attempts DESC"
        else "SELECT * FROM problem WHERE solved = true ORDER BY problem_id ASC"
        val result = query.executeQuery(sentence)
        while (result.next()){
            listOfProblemsSorted.add(
                Problem(
                    result.getInt("problem_id"),
                    result.getString("theme"),
                    result.getString("enunciat"),
                    result.getBoolean("solved"),
                    Attempt(result.getInt("attempts"))
                )
            )
        }
    } catch (e: SQLException){
        println("Error: " + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error: " + e.errorCode + e.message)
    }
    return listOfProblemsSorted
}

private fun getProblemId(): List<Problem> {
    val list = mutableListOf<Problem>()
    val sentence = "SELECT * FROM problem"
    try {
        val query = connection.prepareStatement(sentence)
        val result = query.executeQuery()
        while (result.next()){
            list.add(
                Problem(
                    result.getInt("problem_id"),
                    result.getString("theme"),
                    result.getString("enunciat"),
                    result.getBoolean("solved"),
                    Attempt(result.getInt("attempts"))
                )
            )
        }
    } catch (e: SQLException){
        println("Error: " + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error: " + e.errorCode + e.message)
    }
    return list
}

private fun getScoresInfo(): List<Score> {
    val listOfScores = mutableListOf<Score>()
    val sentence = "SELECT * FROM score"
    try {
        val query = connection.prepareStatement(sentence)
        val result = query.executeQuery()
        while (result.next()){
            listOfScores.add(
                Score(
                    result.getInt("score"),
                    result.getString("teacher_name")
                )
            )
        }
    } catch (e: SQLException){
        println("Error: " + e.errorCode + e.message)
    } catch (e: PSQLException){
        println("Error: " + e.errorCode + e.message)
    }
    return listOfScores
}